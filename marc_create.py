# pymarc is a python library used here to create marc file.
# The method is easy and simple
# The tag 001 is the mandatory field like id in database and provided with id itself here
# The tag 100 is to give author details
# 245 for title
# 520 is for description


from pymarc import Record, Field
import csv

f = open('sample.csv')
csv_f = csv.reader(f)
for row in csv_f:
	record = Record()
	record.add_field(
		Field(
			tag = '001', 
			indicators = ['0','1'],
			subfields = [
				'a', row[0],
			]
		),
		Field(
			tag = '100', 
			indicators = ['0','1'],
			subfields = [
				'a', row[3],
			]
		),
		Field(
			tag = '245', 
			indicators = ['0','1'],
			subfields = [
				'a', row[1],
				'c', 'by ' + row[3]
			]
		),
		Field(
			tag = '520',
			indicators = ['0','1'],
			subfields = [
				'a', row[2]
			]
		)
	)
	out = open('file.mrc', 'ab')
	out.write(record.as_marc())
	out.close()
